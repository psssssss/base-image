# Smoke tests - Display

Smoke test suite for display and UI applications

## These Test Suites includes these tests

### Server Test Suite
1. Test Xorg service is running
2. Test DISPLAY environment variable is set

### UI Test Suite
1. Test neptune3-ui service is running

