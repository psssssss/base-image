#!/bin/bash

readarray -d ' ' -t SERVICES_ARRAY <<< "$SERVICES"
for name in "${SERVICES_ARRAY[@]}"; do
    if ! systemctl is-active "${name%$'\n'}"; then
        exit 1
    fi
done

exit 0

