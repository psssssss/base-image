# base-image

Includes tests for verifying the base images for both Automotive and RHEL/Edge initiatives, images are built using the configurations defined in the ci-cd/maniftests project.

## To run the tests locally
`tmt run plans -n . -vvv`

## To run the tests on provision virtual
`tmt run -a -vvv plans -n . provision --how virtual --image https://cloud.centos.org/centos/9-stream/x86_64/images/CentOS-Stream-GenericCloud-9-20211119.0.x86_64.qcow2`

## To run the tests on provision minute
`tmt run -a -vvv plans -n . provision --how minute --image 1MT-CentOS-Stream-9`

All of these are running all plans in current directory, so running this in e.g. smoke/display would just run all plans in smoke/display directory which for now is server.fmf and ui.fmf

